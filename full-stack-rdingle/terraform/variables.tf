# variables.tf

variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "us-east-2"
}

variable "ecs_task_execution_role_name" {
  description = "ECS task execution role name"
  default = "ryandingle-devTaskExecutionRole"
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}

variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "ryandingle09/ryandingle:0.0.1"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 80
}

variable "app_port_health" {
  description = "Port exposed by the docker image to redirect traffic to health check"
  default     = 8080
}

variable "app_port_front" {
  description = "Port exposed by the docker image to redirect traffic to health check"
  default     = 3000
}

variable "app_port_ssl" {
  description = "Port exposed by the docker image to redirect traffic to health check"
  default     = 443
}

variable "app_port_ssl_arn_cert" {
  description = "Domaine Aws Cert ARN"
  default     = "arn:aws:acm:us-east-2:785572264829:certificate/eb7a2acb-3e6a-4973-b5a7-0bfeb75ce91e"
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 1
}

variable "health_check_path" {
  default = "/"
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "256"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "512"
}

