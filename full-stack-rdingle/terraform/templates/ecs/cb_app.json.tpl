[
  {
    "name": "ryandingle-dev-app",
    "image": "${app_image}",
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/ryandingle-dev-app",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${app_port},
        "hostPort": ${app_port}
      },
      {
        "containerPort": ${app_port_health},
        "hostPort": ${app_port_health}
      },
      {
        "containerPort": ${app_port_front},
        "hostPort": ${app_port_front}
      }
    ]
  }
]
